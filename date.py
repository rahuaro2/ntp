#!/usr/bin/env python

from datetime import datetime
import json
from json import dumps
import time
import subprocess
from subprocess import check_output


#ntp= subprocess.call("ntpstat")
#print("printing NTP")
#print(ntp)

hostname_output = check_output(["/rootfs/usr/bin/hostname"])
print("hostname_output")
print(hostname_output)


ntp_output = check_output(["/rootfs/usr/bin/ntpstat"])
print("out")
print(ntp_output)

sync_status=""

if ntp_output.find("synchronised") == -1:
 print("Out of sync")
 sync_status="out of sync"
else:
 print("In sync")
 sync_status="in sync"


date_output = check_output(["/rootfs/usr/bin/date"])
print("date_output")
print(date_output)


tmz_output = ""
if date_output.find("GMT") == -1:
 print("Out of timezone")
 tmz_status=("out of timezone")
else:
 print("In Timezone")
 tmz_status=("In timezone")

ntp_output = {}
values=[sync_status,tmz_status]
ntp_output[hostname_output]= values
print(ntp_output)


